
const { ComponentDialog, WaterfallDialog} = require('botbuilder-dialogs');
const { TextPrompt } = require('botbuilder-dialogs');

const CLIMACIUDAD_PROMPT = 'climaCiudadPrompt';

class ClimaDialog extends ComponentDialog {
	static get Name() { return CLIMA_DIALOGID; }
	constructor(dialogId, conversationState, userState, userProfileAccessor) {
		 super(dialogId);
		 
		 // verificar params
		 if (!dialogId) throw ('Missing parameter.  dialogId is required');
		 if (!userProfileAccessor) throw ('Missing parameter.  userProfileAccessor is required');
		 
		 // save for later
		 this.userProfileAccessor = userProfileAccessor;
		 
		// waterfall dialogs
		this.addDialog(new WaterfallDialog('climaMain', [
            this.step1.bind(this),
            this.step2.bind(this)
        ]));	
		
		// agregar dialogos usados
		this.addDialog(new TextPrompt(CLIMACIUDAD_PROMPT, this.validateCity));

		 
	}
	
	// recibe info de contexto del paso actual
	async step1(step) {
		const userProfile = await this.userProfileAccessor.get(step.context);
		return await step.prompt(CLIMACIUDAD_PROMPT, `don ${userProfile.name}, De que ciudad quiere su cochino clima?`);
	}
	
	async step2(step) {
		const step1_city = step.result;
		
		await step.context.sendActivity(`ciudad ${step1_city}`)
		return await step.endDialog();
	}
	
}

exports.ClimaDialog = ClimaDialog;