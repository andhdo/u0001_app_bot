var zipFolder = require('zip-folder');
var path = require('path');
var fs = require('fs');
var request = require('request');

var rootFolder = path.resolve('.');
var zipPath = path.resolve(rootFolder, '../u0001a1-bot-app.zip');
var kuduApi = 'https://u0001a1-bot-app.scm.azurewebsites.net/api/zip/site/wwwroot';
var userName = '$u0001a1-bot-app';
var password = 'jW3gtgqMnGxrxuY8p1YKlJlaa9XgrPjYBqb5rqqFXhZHnDkf2NfNB7nqsg3c';

function uploadZip(callback) {
  fs.createReadStream(zipPath).pipe(request.put(kuduApi, {
    auth: {
      username: userName,
      password: password,
      sendImmediately: true
    },
    headers: {
      "Content-Type": "applicaton/zip"
    }
  }))
  .on('response', function(resp){
    if (resp.statusCode >= 200 && resp.statusCode < 300) {
      fs.unlink(zipPath);
      callback(null);
    } else if (resp.statusCode >= 400) {
      callback(resp);
    }
  })
  .on('error', function(err) {
    callback(err)
  });
}

function publish(callback) {
  zipFolder(rootFolder, zipPath, function(err) {
    if (!err) {
      uploadZip(callback);
    } else {
      callback(err);
    }
  })
}

publish(function(err) {
  if (!err) {
    console.log('u0001a1-bot-app publish');
  } else {
    console.error('failed to publish u0001a1-bot-app', err);
  }
});